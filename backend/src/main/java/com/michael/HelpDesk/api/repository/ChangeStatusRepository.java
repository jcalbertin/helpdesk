package com.michael.HelpDesk.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.michael.HelpDesk.api.entity.ChangeStatus;

public interface ChangeStatusRepository extends MongoRepository<ChangeStatus, String> {
	
	public Iterable<ChangeStatus> findByTicketIdOrderByDateChangeStatusDesc(String ticketId);

}
