package com.michael.HelpDesk.api.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.michael.HelpDesk.api.entity.Ticket;

public interface TicketRepository extends MongoRepository<Ticket, String> {

	public Page<Ticket> findByUserIdOrderByDateDesc(Pageable pages, String userId);
	
	public Page<Ticket> findByTitleIgnoreCaseContainingAndStatusAndPriorityOrderByDateDesc(
			String title, String status, String priority, Pageable pages);
	
	public Page<Ticket> findByTitleIgnoreCaseContainingAndStatusAndPriorityAndUserIdOrderByDateDesc(
			String title, String status, String priority, Pageable pages);
	
	public Page<Ticket> findByTitleIgnoreCaseContainingAndStatusAndPriorityAndAssignedUserOrderByDateDesc(
			String title, String status, String priority, Pageable pages);
	
	public Page<Ticket> findByNumber(Integer number, Pageable pages);
}
